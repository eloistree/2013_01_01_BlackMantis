<?php
	# Mantis - a php based bugtracking system
	# Copyright (C) 2000 - 2002  Kenzaburo Ito - kenito@300baud.org
	# Copyright (C) 2002 - 2006  Mantis Team   - mantisbt-dev@lists.sourceforge.net
	# This program is distributed under the terms and conditions of the GPL
	# See the README and LICENSE files for details
 
	# --------------------------------------------------------
	# $Id: $
	# --------------------------------------------------------
 
	require_once( 'core.php' );
 
	access_ensure_project_level( VIEWER );   // change minimum access level if required.
 
	$t_chat_path = dirname( dirname( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'Chat' . DIRECTORY_SEPARATOR;  // adjust to your own path
 
	require_once ( $t_chat_path . 'src' . DIRECTORY_SEPARATOR . 'phpfreechat.class.php' );
 
	$t_project_name = project_get_name( helper_get_current_project() );
	$t_nick = user_get_name( auth_get_current_user_id() );
	
	$params['isadmin'] = user_is_administrator( auth_get_current_user_id() ); 
 
	$params["serverid"] = md5( __FILE__ ); 	
	$params["title"]     = "Communication";//$t_project_name;
	//$params["channels"]     = $t_project_name;
	$params["nick"]     = $t_nick;
	$params["frozen_nick"] = true;
	$params["refresh_delay"] = 5000;
	$params["clock"] = true;
 
	$chat = new phpFreeChat( $params );
 
	html_page_top1();
	echo '<meta http-equiv="content-type" content="text/html; charset=utf-8" />';
	$chat->printJavascript();
	$chat->printStyle();
	html_page_top2();
 
	# uncomment the following line if you are using Mantis 1.1.0a1 or above.
	# print_recently_visited();
 
	echo '<br />';
 
	$chat->printChat();
 
	html_page_bottom1( __FILE__ );
?>