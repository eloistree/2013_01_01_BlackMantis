<?php
# MantisBT - a php based bugtracking system

# MantisBT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# MantisBT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MantisBT.  If not, see <http://www.gnu.org/licenses/>.

	/**
	 * This is the first page a user sees when they login to the bugtracker
	 * News is displayed which can notify users of any important changes
	 *
	 * @package MantisBT
	 * @copyright Copyright (C) 2000 - 2002  Kenzaburo Ito - kenito@300baud.org
	 * @copyright Copyright (C) 2002 - 2012  MantisBT Team - mantisbt-dev@lists.sourceforge.net
	 * @link http://www.mantisbt.org
	 */
	 /**
	  * MantisBT Core API's
	  */
	require_once( 'core.php' );

	require_once( 'current_user_api.php' );
	require_once( 'news_api.php' );
	require_once( 'date_api.php' );
	require_once( 'print_api.php' );
	require_once( 'rss_api.php' );

	access_ensure_project_level( VIEWER );

	$f_offset = gpc_get_int( 'offset', 0 );

	$t_project_id = helper_get_current_project();

	$t_rss_enabled = config_get( 'rss_enabled' );

	if ( OFF != $t_rss_enabled && news_is_enabled() ) {
		$t_rss_link = rss_get_news_feed_url( $t_project_id );
		html_set_rss_link( $t_rss_link );
	}

	html_page_top(  );

	

	echo '<br />';
	echo '<br />';

?>
<h1>Newbie<h1>
<h2>Quick, I just need to report a stuff</h2>
<p> Try to always keep in mind that if you report a issue, you want that someone fix it. So do not be lazy on description and be clear. Thus, the issue will be easy to fix. </p>
<p> Be sure of what you report. A wrong information will lead to lose the developers'time.</p>
<table >
<tr><td class="form-title" colspan="3">Main fields<td></tr>
	<tr class="row-1"> 
		<td class="category"><strong>Category</strong></td><td>What kind of issue do you found</td><td></td>
	</tr>
	
	<tr class="row-1"> 
		<td class="category"><strong>Reproducibility</strong></td><td>How often does the description of the issue happened?</td><td>Always</br>Sometimes</br>Random</br>Unable to reproduce</br>Have not tried</br></td>
	</tr>
	
	<tr class="row-1"> 
		<td class="category"><strong>Severity</strong></td><td>Does the issue is .. ?</td><td>A - a crash or a blocker</br>B - a issue to change for the release</br>C - a issue that can be fixed in an update</br>D - a suggestion</td>
	</tr>
	
	<tr class="row-1"> 
		<td class="category"><strong>Assign To</strong></td><td>Does you know who have to work on that ?</td> <td>Do not use, except in a hurry</td>
	</tr>
	
	<tr class="row-1"> 
		<td class="category"><strong>Summary</strong></td><td>What is the issue ?</br>Convention(Severity: Category: Summary)</td><td></td>
	</tr>
	<tr class="row-1"> 
		<td class="category"><strong>Description</strong></td><td>The reader does not understand Summary and Steps, can you be much explicit ?</td><td></td>
	</tr>
	<tr class="row-1"> 
		<td class="category"><strong>Steps to reproduce</strong></td><td>As a newbie, what should I do to reproduce the bug ?</td><td>1. Launch the app on Android</br>2. Go in ..>..>..</br>3. Do this</br>4. Observe that </br>5. Notice this</td>
	</tr>
	
	<tr class="row-1"> 
		<td class="category"><strong>Upload File</strong></td><td>Use this field, if you want to link a document as a screenshot.</td><td></td>
	</tr>
</table>

<h2>How can I impove my skills at reporting an issue</h2>
<h3>Tips</h3>
<ol>
	<li>Do not use "I, me...", you do not existe. Instead, describe what the player have to do to found the issue</li>
	<li>Always read back your issue and try to be at the reader place. Do you think he will understand what you wrote ? </li>
	<li>Do not forget that what you want is to help developpers to impove the quality of them application</li>
	<li>Use the same vocabulary as your coworker</li>
	<li>Do not change of vocabulary in the middle of your issue</li>
    <li>Be sure of what you notify. You can lost times but not the developer that will fix the issue</li>
</ol>

<h2>By the way, how does really work Mantis ?</h2>
<h3> First Steps</h3>
<p><i><b>0:00</b>=Introduction,  <b>0:23</b>=What is Mantis,  <b>1:38</b>=Inscription,  <b>2:27</b>=Description,  <b>4:20</b>=First report,  <b>6:30</b>=Upload file,  END</i></p>
<a href="http://www.youtube.com/watch?v=elCmtmn5cKc"><img src="/Style/Mantis/youtube_mantis.PNG" width="640" height="auto" alt="Video Youtube: Tutorial sur Mantis"/></a>
<!--

<h3>Behind the interface</h3>
<p>Explain the BD, config file, structure ... </p>

<h3>Vocabulary</h3>
<p></p>
<h3>Convention</h3>
<p></p>-->
<?php
	html_page_bottom();
?>
