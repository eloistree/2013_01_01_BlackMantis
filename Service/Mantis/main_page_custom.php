<h1>Hello Mantis !</h1>
<H2>What is Mantis</h2>
<p>Source: <a href="http://en.wikipedia.org/wiki/Mantis_Bug_Tracker">Wiki</a>, <a href="http://www.youtube.com/embed/elCmtmn5cKc">YouTube</a></p><p>Mantis Bug Tracker is a free and open source, web-based bug tracking system released under the terms of the GNU General Public License version 2. The most common use of MantisBT is to track software defects. However, MantisBT is often configured by users to serve as a more generic issue tracking system and project management tool.

The name Mantis and the logo of the project refer to the Mantidae family of insects, known for the tracking of and feeding on other insects, colloquially referred to as "bugs". The name of the project is typically abbreviated to either MantisBT or just Mantis.
</p>

<h2>Links</h2>
<ul>
	<li>
		<a href="http://www.mantisbt.org/manual/index.php">Manuel</a>
	</li>
	<li>
		<a href="http://symfo.web.fc2.com/mantis-doc/MantisBT/_config_defaults_inc.php.html">API documentation</a>
	</li>
	<li>
		<a href="http://bugtracker.morinie.fr/mantis/plugins_list_page.php">Plug-in</a>
	</li>
	
	<li>
		<a href="http://www.futureware.biz/mantisconnect/">SOAP webservice on Mantis API</a>
	</li>

</ul>
