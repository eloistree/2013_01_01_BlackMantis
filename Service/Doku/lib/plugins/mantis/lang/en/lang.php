<?php

$lang['message'] = 'Entry from <a href="[url]">Mantis</a>.';
$lang['nosoap']   = 'Sorry, no SOAP available on that system.';
$lang['noconfig']   = 'Please configure the plugin via the admin area first.';
$lang['noprojectname']   = 'You have to enter a project name.';
$lang['projectnotfound']   = 'No project with the name <b>[project]</b> could be found.';
$lang['accessdenied']   = 'Access to <a href="[url]">Mantis</a> denied. Maybe wrong username/password?';
$lang['noissuesfound']   = 'No issues were found!';

$lang['table_summary']   = 'Summary';
$lang['table_reporter']   = 'Reporter';
$lang['table_description']   = 'Description';
