<?php

$lang['message'] = 'Eintrag aus <a href="[url]">Mantis</a>.';
$lang['nosoap']   = 'Leider ist kein SOAP auf diesem System verfügbar.';
$lang['noconfig']   = 'Bitte konfigurieren Sie das Plugin zuerst über die Administrations-Oberfläche.';
$lang['noprojectname']   = 'Es wurde kein Projektname angegeben.';
$lang['projectnotfound']   = 'Es konnte kein Projekt mit dem Namen <b>[project]</b> gefunden werden.';
$lang['accessdenied']   = 'Zugang zu <a href="[url]">Mantis</a> wurde verweigert.';
$lang['noissuesfound']   = 'Es wurden keine anzuzeigenden Einträge gefunden.';

$lang['table_summary']   = 'Zusammenfassung';
$lang['table_reporter']   = 'Melder';
$lang['table_description']   = 'Beschreibung';