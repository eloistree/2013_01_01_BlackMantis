====== Hello Documentation ======
Bienvenu sur la partie documentation de mon site web.
Ici, est stocké les données propres aux
  * Applications développées
  * Tutoriels, rappelles...
  * Informations, news...

//Ce wiki est en développement et est à l'état de brouillon, je ne garantis pas encore la qualité, la structure ou l'orthographe, mais je  travaillerai dans le but d'avoir un version correct pour la fin Janvier.\\
//''//(J'utilise DokuWiki qui appartient à [[http://www.splitbrain.org/projects/dokuwiki| Andreas Gohr]])//''\\


//Si malgré ça vous désirez accéder à ma documentation, contactez-moi: streeeloi@gmail.com.//

------------------------------------------------------------

===== Site web =====
[[http://gamerkitapplication.be|Portfolio]]
[[http://gamerkitapplication.be/mantis/bug_report_page.php|Bug tracker]]


===== Severs =====
TS3: 178.170.111.64

------------------------------------------------------------
===== Ma documentation =====
==== Logiciels et sites web ====

[[docs:mantis|Mantis]]
[[docs:dokuwiki|DokuWiki]]

==== Tutoriels ====
<del>[[tutoriels:be_a_tester|Be a tester]]</del>


==== Video games ====

[[games:donjonDefender|Donjon Defender]]


[[games:FTL|FTL: Fast then light]]

------------------------------------------------------------
===== Programmation  =====
==== Projects Library ====
<del>[[developement:projets|Projets en développement]]</del>
==== Codes Library ====
[[programmation:manoeuvre|Manoeuvre classique]]
==== Synthèses ====
<del>[[programmation:java|Java]]</del>
<del>[[programmation:android|Android]]</del>
<del>[[programmation:c++|c++]]</del>



----
===== Recherche=====
==== Testing ====
<del>[[study:TDD|TDD]]</del> <del>[[study:BDD|BDD]]</del><del> [[study:SCRUM|SCRUM]]</del>

<del>[[study:Cucumber|Cucumber]]</del> <del>[[study:Calabash|Calabash]]</del>
==== Mantis/DokuWiki/TDD ====
<del>[[study:linkMantisDokuWikiTDD|Comment lier l'ensemble]]</del>


==== Soup de mots====
[[study:soup:culture|Culture]]\\
[[study:soup:loisir|Loisir]]
----
===== Média=====
[[media:videos|Vidéos]]\\



----


===== Admin =====
[[admin:data|Tavern]]\\
[[admin:projets|Etat d'avancement]]\\
[[admin:casiers|Casiers]]


