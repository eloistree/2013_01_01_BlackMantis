a:7:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:" ﻿";}i:2;i:1;}i:3;a:3:{i:0;s:4:"html";i:1;a:1:{i:0;s:6159:"<h1 class="titre" >Mes liens</h1>



<h1></h1>
<ul><li><strong>Devises</strong> <a href="http://www.phraseculte.fr/">http://www.phraseculte.fr/</a></li>
<li><strong>CSS3 bootstrap</strong> <a href="http://twitter.github.com/bootstrap/">http://twitter.github.com/bootstrap/</a></li>
<li><strong>primeface (JSF)</strong> <a href="http://www.primefaces.org/">http://www.primefaces.org/</a></li></ul>

<h2>Android</h2>
<ul><li><strong>Statistique</strong> <a href="http://developer.android.com/about/dashboards/index.html">http://developer.android.com/</a></li>
<li><strong>Blog android</strong> <a href="http://www.ace-art.fr/wordpress/category/programmation/android-programmation/">http://www.ace-art.fr/</a></li>
</ul>
<h2>Realitée augmentée</h2>
<ul>
<li><strong>Java Library </strong> <a href="http://nyatla.jp/nyartoolkit/wp/?page_id=198">http://nyatla.jp/nyartoolkit/wp/?page_id=198</a></li>
<li><strong>Un exemple</strong> <a href="http://www.creativeapplications.net/processing/augmented-reality-with-processing-tutorial-processing/">http://www.creativeapplications.net/</a></li>

</ul>


<h2>Travailler dans le monde du jeu vidéo</h2>
<ul><li><strong>Map</strong> <a href="http://www.gamedevmap.com/index.php?tool=location&query=Brussels">http://www.gamedevmap.com/</a></li>
<li><strong>Jobs</strong> <a href="http://emploi.afjv.com/index.php">http://emploi.afjv.com/index.php</a></li></ul>




<h2>Java - Game</h2>

<ul><li><strong>Killer game programming</strong> <a href="http://fivedots.coe.psu.ac.th/~ad/jg/">http://fivedots.coe.psu.ac.th/~ad/jg/</a></li>
</ul>
<h2>API</h2>

<ul>
<li><strong>Java SE 7</strong>:     <a href="http://docs.oracle.com/javase/7/docs/api/">http://docs.oracle.com/javase/7/docs/api/</a></li>
<li><strong>Java EE</strong>:     <a href="http://docs.oracle.com/javaee/1.4/api/">http://docs.oracle.com/javaee/1.4/api/</a></li>
<li></li>
<li><strong>JavaFX</strong>: <a href="http://docs.oracle.com/javafx/2/api/index.html">http://docs.oracle.com/javafx/2/api/index.html</a></li>
<li><strong>JavaFX</strong> CSS:  <a href="http://docs.oracle.com/javafx/2/api/javafx/scene/doc-files/cssref.html">http://docs.oracle.com/javafx/2/api/javafx/scene/doc-files/cssref.html</a></li>
</ul>
<h1>Liens utiles</h1>
<h2>&nbsp;</h2>
<h2>Conf&eacute;rences vid&eacute;o</h2>
<ul>

<li><strong>Parley</strong>: <a href="http://www.parleys.com">http://www.parleys.com</a>   <i>(*****)</i></li>
<li><strong>Devoxx</strong>: <a href="https://cfp.devoxx.com">https://cfp.devoxx.com/</a>   <i>(***)</i></li>

<li><strong>Appliction de vue javaFX</strong>: <a href="http://fxexperience.com/">http://fxexperience.com/</a> </li>


</ul>

<h2>Share</h2>
<ul>
<li><strong>SlideShare</strong>: <a href="http://www.slideshare.net/">http://www.slideshare.net/</a>  </li>

</ul>


<h2>Plug-in Eclipse</h2>
<ul>
<li><strong>Uml viewer</strong>: <a href="http://www.objectaid.com/class-diagram">http://www.objectaid.com/class-diagram</a>  </li>
<li><strong>Mercurial</strong>: <a href="http://javaforge.com/project/HGE">http://javaforge.com/project/HGE</a>  </li>
<li><strong>Google GWT</strong>: <a href="https://developers.google.com/eclipse/">https://developers.google.com/eclipse/</a>  </li>

</ul>

<h2>Tutoriaux</h2>
<ul>
<li><strong>Java SE 7</strong>:     <a href="http://docs.oracle.com/javase/tutorial/index.html">http://docs.oracle.com/javase/tutorial/index.html</a></li>
<li><strong>JavaFX</strong>: <a href="http://docs.oracle.com/javafx/ ">http://docs.oracle.com/javafx/ </a> </li>
<li><strong>W3School</strong>: <a href="http://www.w3schools.com/">http://www.w3schools.com/ </a></li> 
<li><strong>J2EE</strong>: <a href="http://docs.oracle.com/javaee/6/tutorial/doc/gexaf.html">http://docs.oracle.com/javaee/6/tutorial/doc/gexaf.html</a></li> 

<li><strong>Java tips</strong>: <a href="http://www.jtips.info/index.php?title=Accueil">http://www.jtips.info/index.php?title=Accueil</a> </li>

<li><strong>GreenFoot</strong>: <a href="http://www.greenfoot.org/">http://www.greenfoot.org/</a>(programmation pour jeune) </li>
<li><strong>Tuto avec GreenFoot</strong>: <a href="http://blogs.kent.ac.uk/mik/category/joy-of-code/">http://blogs.kent.ac.uk/mik/category/joy-of-code/</a> </li>
</ul>

<h2>Games</h2>
<h3>League of legend</h3>

<ul>
<li><strong>Inscription</strong>: <a href="http://signup.leagueoflegends.com/?ref=4dbdfaa13f010679585444">http://signup.leagueoflegends.com/</a></li>
<li><strong>PBE</strong>: <a href="https://pbe.leagueoflegends.com/fr/signup/index">https://pbe.leagueoflegends.com/fr/signup/index</a> (permet de tester les champions avant la sortie)</li>
  <li><strong>Champion Select</strong>: <a href="http://www.championselect.net/">http://www.championselect.net/</a></li>
  
</ul>
<h2>Ubuntu</h2>
<ul>
  <li><strong>Tomcat</strong>: <a href="http://doc.ubuntu-fr.org/tomcat">http://doc.ubuntu-fr.org/tomcat</a></li>
  <li><strong>PostgeSQL</strong>: <a href="http://doc.ubuntu-fr.org/postgresql">http://doc.ubuntu-fr.org/postgresql/</a></li>
  <li><strong>Nano</strong>: <a href="http://doc.ubuntu-fr.org/nano">http://doc.ubuntu-fr.org/nano</a></li>
  <li><strong>Wget</strong>: <a href="http://doc.ubuntu-fr.org/wget">http://doc.ubuntu-fr.org/wget</a></li>
</ul>
<p>&nbsp;</p>
<h2>Utile</h2>
<ul>
<li><strong>Youtube to mp3</strong>: <a href="http://www.youtube-mp3.org/fr">http://www.youtube-mp3.org/fr</a></li>
<li><strong>Devise de programmeur</strong>:<a href="http://en.wikiquote.org/wiki/Programming">http://en.wikiquote.org/wiki/Programming</a></li>
</ul>

<h2>Tuto android à lire</h2>
<ul>
<li><strong>Développer.com</strong>: <a href="http://android.developpez.com/cours/?page=Debutant#Debut">http://android.developpez.com/cours/</a></li>
<li><strong>Parley: intro Android</strong>:  <a href="http://www.parleys.com/#st=5&id=387&sl=1">http://www.parleys.com/#st=5&id=387&sl=1</a></li>
<li><strong>Site du zéro</strong>:  <a href="http://www.siteduzero.com/tutoriel-3-554364-creez-des-applications-pour-android.html">http://www.siteduzero.com/</a></li>

</ul>

<h2>Crâneur</h2>
<ul>
<li><strong>Hacker typer</strong>: <a href="http://hackertyper.com/">http://hackertyper.com/</a></li>
</ul>

  
</div>";}i:2;i:11;}i:4;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:0:"";}i:2;i:6177;}i:5;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:6177;}i:6;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:6177;}}